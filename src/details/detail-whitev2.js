/**
 * BLOCK: block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/detail-whitev2', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Detail item, white BG, left picture. Version 2' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	parent: ['trilogo/details-block'],
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
        mediaLil: {
            type: 'object',
            default: {},
        },
		title: {
            type: 'string'
        }, 
        tip: {
			type: 'string'
        },
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        anchor: {
            type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, mediaLil, title, tip, greenName, greenURL, anchor, greenTRG, greenModal }, setAttributes} = props;
        const TEMPLATE = [[ 'core/list', {}]];
        var onSelectImageLil = function (mediaLil) {
			props.setAttributes({ mediaLil: mediaLil })
		};
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
        };
        const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
        const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};
		const onChangeAnchor = ( newAnchor ) => {
            props.setAttributes( { anchor: newAnchor } );
        };
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (  
            <div  className="details__item">
                <TextControl
                    tagName="span"		
                    className="anchor-link"
                    placeholder={ __('Укажите якорь') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeAnchor}
                    value={ anchor }/>
                <div className="container">                        
                    <div className="col col-img">                                            
                        <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                            style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                                <MediaUpload
                                onSelect={ onSelectImage }
                                type='image'
                                value={ media.id} 
                                render={ function(obj) {
                                    return <Button
                                    className={ media.id ? 'image-button' : 'button button-large' }
                                    onClick={ obj.open }>
                                        { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                                    </Button>
                                    }
                                }/>
                        </div>
                        <div className={ mediaLil.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                            style={ mediaLil.id ? { Image: 'url(' + mediaLil.url + ')' } : {} }>
                                <MediaUpload
                                label="Icon"
                                onSelect={ onSelectImageLil }
                                type='image'
                                value={ mediaLil.id} 
                                render={ function(obj) {
                                    return <Button
                                    className={ mediaLil.id ? 'image-button' : 'button button-large' }
                                    onClick={ obj.open }>
                                        { !mediaLil.id ? __('Upload Image')  : <img src={ mediaLil.url } alt={ mediaLil.alt }/> }
                                    </Button>
                                    }
                                }/>
                        </div>
                    </div>
                    <div className="col col-cont">
                        <TextControl
                            tagName="span"	
                            className="block-title"				
                            placeholder={ __('Напишите заголовок блока') }
                            keepPlaceholderOnFocus= {true}
                            onChange= {onChangeTitle}
                            value={ title }/>
                        <TextControl
                            tagName="p"	
                            className="block-desc"	
                            placeholder={ __('Напишите описание блока') }
                            keepPlaceholderOnFocus= {true}
                            onChange= {onChangeTip}
                            value={ tip }/>
                        <InnerBlocks 
                            allowedBlocks={ ['core/list'] }
                            template={ TEMPLATE } />
                        <div className="buttons">
                            <ToggleControl 
                                label="Modal window"
                                checked={ greenModal }
                                className="link-url"
                                onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
                                <TextControl
                                    tagName="span"		
                                    className="link-url green-link"
                                    placeholder={ __('Укажите ссылку на страницу') }
                                    keepPlaceholderOnFocus= {true}
                                    onChange= {onChangeGreenURL}
                                    value={ greenURL }/>
                                <ToggleControl 
									checked={ greenTRG }
									className="link-url"
									onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
                            </div>
                            <TextControl
                                tagName="span"		
                                className="link-name green-name"			
                                placeholder={ __('Попробовать бесплатно') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeGreenName}
                                value={ greenName }/>	
                        </div>
                    </div>   
                </div>
            </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { media, mediaLil, title, tip, greenName, greenURL, anchor, greenTRG, greenModal }} = props;
		return (
            <div  className="details__item" id={ anchor }>
                <div className="container">
                    <div className="details__item-wrap">                        
                        { media.id ?
                        <div className="col col-img non-reverse">
                            <img src={ media.url } alt={ media.alt ? media.alt : 'ASO'} className="item__image big-screen"/>
                            { mediaLil.id ?
                            <img src={ mediaLil.url } alt={ mediaLil.alt ? mediaLil.alt : '' } className="icon"/>: ''}
                        </div> : ''}                       
                        { greenName ?                       
                        <div className="details__button-mobile">
                            { greenModal ? 
                            <button data-micromodal-trigger="modal-1" className="green-link">{ greenName }
                                <span className="try-free-mobile">
                                    <i className="ic-arrow-right-white"></i>
                                </span>
                            </button> :
                            greenURL && !greenTRG ?
                            <a href={ greenURL } className="green-link">{ greenName }
                                <span className="try-free-mobile">
                                    <i className="ic-arrow-right-white"></i>
                                </span>
                            </a> :
                            greenURL && greenTRG ?
                            <a href={ greenURL } className="green-link"  target="_blank" rel="noopener noreferrer">{ greenName }
                                <span className="try-free-mobile">
                                    <i className="ic-arrow-right-white"></i>
                                </span>
                            </a> : ''}
                        </div> : ''}      
                        <div className="col col-cont">
                            { title ?
                            <span className="item__title">{ title }</span>: ''}
                            { tip ?
                            <p className="item__subtitle">{ tip }</p>: ''}
                            <InnerBlocks.Content />	
                            { greenName ?                              
                            <div className="item__button">
                                { greenModal ?
                                <button data-micromodal-trigger="modal-1" className="green-link">{ greenName }
                                    <span className="try-free-mobile">
                                        <i className="ic-arrow-right-white"></i>
                                    </span>
                                </button> : 
                                greenURL && !greenTRG ?
                                <a href={ greenURL } className="green-link">{ greenName }
                                    <span className="try-free-mobile">
                                        <i className="ic-arrow-right-white"></i>
                                    </span>
                                </a> :
                                greenURL && greenTRG ?
                                <a href={ greenURL } className="green-link" target="_blank" rel="noopener noreferrer">{ greenName }
                                    <span className="try-free-mobile">
                                        <i className="ic-arrow-right-white"></i>
                                    </span>
                                </a> : '' }
                            </div> : ''}
                        </div>
                    </div>
                </div>
            </div>                   
		);
	},
} );
