/**
 * BLOCK: block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

const MediaUpload = wp.blockEditor.MediaUpload;
const Button = wp.components.Button;
const RichText = wp.blockEditor.RichText;
const TextControl = wp.components.TextControl;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/faq-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO FAQ item' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	parent: ['trilogo/faq-block', 'trilogo/rates-faq-block'],
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'string'
        },
        tip: {
			type: 'array',
			source: 'children',
			selector: 'p'
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, tip }} = props;

        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
        };
        const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};

		return (
            <div className="faq__item">
                <div className="container">
                    <TextControl
						tagName="span"
						className="question"
                        placeholder={ __('Напишите вопрос') }
                        keepPlaceholderOnFocus= {true}
                        onChange= {onChangeTitle}
                        value={ title }/>
                    <RichText
                        tagName="p"
                        placeholder={ __('Напишите ответ') }
                        keepPlaceholderOnFocus= {true}
                        onChange= {onChangeTip}
                        value={ tip }/>
                </div>
            </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		return (
            <div className="faq__item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                <div className="faq__heading">
                    <div className="faq__question">
                        <a className="faq__question-link" href="#">
							<span  itemprop="name">{ props.attributes.title }</span>
                        </a>
                        <div className="faq__btn">
                            <span className="faq__btn-line faq__btn-line_before"></span>
                            <span className="faq__btn-line faq__btn-line_after"></span>
                        </div>
                    </div>
                </div>
                <div className="faq__body" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
                    <RichText.Content
                        tagName='p'
						itemprop="name"
                        className="faq__answer"						
						value={ props.attributes.tip } />
                </div>
            </div>
		);
	},
} );
