/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/rates-faq-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO FAQ block with top border' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
		},
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const { attributes: { title, greenName, greenURL, greenTRG, greenModal }, setAttributes} = props;
        const TEMPLATE = [[ 'trilogo/faq-item', {}]];
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
        };
        const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};
		return (
            <div className="faq">
                <div className="container">
                    <RichText
						tagName="span"	
						className="page-title"
						placeholder={ __('Введите заголовок блока') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
                    <InnerBlocks 
                        allowedBlocks={ ['trilogo/faq-item'] }
                        template={ TEMPLATE } />
                    <div className="buttons">
						<ToggleControl 
							label="Modal window"
							checked={ greenModal }
							className="link-url"
							onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
						<div className="url-group">
							<IconButton icon="admin-links"
								className="link-btn"></IconButton>
							<TextControl
								tagName="span"	
								className="link-url"	
								placeholder={ __('Ссылку на страницу') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeGreenURL}
								value={ greenURL }/>
							<ToggleControl 
								checked={ greenTRG }
								className="link-url"
								onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
						</div>
						<TextControl
							tagName="span"		
							className="link-name green-link"			
							placeholder={ __('Узнать больше') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeGreenName}
							value={ greenName }/>
					</div>
                </div>                
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, greenName, greenURL, greenTRG, greenModal }, setAttributes} = props;
		return (
            <div className="faq faq-rates">
                <div className="container">
                    <RichText.Content 
                        tagName='span' 
                        className="page-title"
						value={ title } />
                    <div className="faq__block">
                        <InnerBlocks.Content />
                    </div>
					{ greenName ?
                    <div className="faq__button">
                        { greenModal ? 
                        <button data-micromodal-trigger="modal-1" className="green-link">{ greenName }<i className="ic-arrow-right-white"></i></button> :
                        greenURL && !greenTRG ?
                        <a href={ greenURL } className="green-link">{ greenName }<i className="ic-arrow-right-white"></i></a> : 
                        greenURL && greenTRG ?
                        <a href={ greenURL } className="green-link"  target="_blank" rel="noopener noreferrer">{ greenName }<i className="ic-arrow-right-white"></i></a> : '' }
                    </div> : '' }   
                </div>
            </div>
		);
	}
} );
