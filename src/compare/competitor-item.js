/**
 * BLOCK: block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/competitor-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Competitor' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	parent: ['trilogo/competitor-block'],
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		comp: {
			type: 'string'
        },
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { comp, greenName, greenURL, greenTRG, greenModal }, setAttributes} = props;
        
        const onChangeComp = ( newComp ) => {
            props.setAttributes( { comp: newComp } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
        };
		return (  
            <div className="comp__item">
                <TextControl
					tagName="span"	
					className="comp-name"	
                    placeholder={ __('Введите название конкурента') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeComp}
                    value={ comp }/>			
				<div className="buttons">
					<ToggleControl 
						label="Modal window"
						checked={ greenModal }
						className="link-url"
						onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
					<div className="url-group">
						<IconButton icon="admin-links"
							className="link-btn"></IconButton>								
						<TextControl
							tagName="span"	
							className="link-url watch-link"	
							placeholder={ __('Ссылка') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeGreenURL}
							value={ greenURL }/>
						<ToggleControl 
							checked={ greenTRG }
							className="link-url"
							onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
					</div>	
					<TextControl
						tagName="span"		
						className="link-name watch-tools"
						placeholder={ __('Альтернатива') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeGreenName}
						value={ greenName }/>
				</div>	
            </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { comp, greenName, greenURL, greenTRG, greenModal }} = props;
		return (
			<div className="comp__wrap">
				<div className="comp__item">
					<div className="comp__name">
						<span className="comp__item-title">ASOMobile</span>
						<img src="/wp-content/themes/aso/images/dest/vs.svg" alt="VS" className="vs"/>
						<span className="comp__item-title">{ comp }</span>
					</div>
						{ greenName && greenURL && !greenTRG ?
						<button data-micromodal-trigger="modal-1" className="comp__item-link">{ greenName }</button> :
						greenName && greenURL && !greenTRG ?
						<a href={ greenURL } className="comp__item-link">{ greenName }</a> : 
						greenName && greenURL && greenTRG ?
						<a href={ greenURL } className="comp__item-link"  target="_blank" rel="noopener noreferrer">{ greenName }</a> : ''}
				</div>    
			</div>            
		);
	},
} );

        