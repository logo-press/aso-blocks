/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
const el = wp.element.createElement;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/table-row-2', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Table row' ), // Block title.
    parent: ['trilogo/table-block-2'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'string'
		},
		cell1: {
			type: 'string'
		},
		cell2: {
			type: 'string'
		},
		toggle1: {
			type: 'boolean',
			default: true
		},
		toggle2: {
			type: 'boolean',
			default: true
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: {  
			title, 
			cell1,
			cell2,
			toggle1,
			toggle2,
		 }, setAttributes} = props;
		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeCell1 = ( newCell1 ) => {
            props.setAttributes( { cell1: newCell1 } );
		};
		const onChangeCell2 = ( newCell2 ) => {
            props.setAttributes( { cell2: newCell2 } );
		};
		return (
            <div className="compare__row">
				<TextControl
					tagName="span"					
					placeholder={ __('Заголовок строки') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangeTitle}
					value={ title }/>
				<div className="cell-container">
					<div className="compare__row-cell">
						<TextControl
							tagName="span"	
							className="off-tip"	
							placeholder={ __('Текстовое содержимое ячейки') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeCell1}
							value={ cell1 }/>
						<ToggleControl 
							checked={ toggle1 }
							className="link-url"
							onChange={(newToggle1) => setAttributes({ toggle1: newToggle1})} />
					</div>
					<div className="compare__row-cell">
						<TextControl
							tagName="span"	
							className="off-tip"	
							placeholder={ __('Текстовое содержимое ячейки') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeCell2}
							value={ cell2 }/>
						<ToggleControl 
							checked={ toggle2 }
							className="link-url"
							onChange={(newToggle2) => setAttributes({ toggle2: newToggle2})} />
					</div>
				</div>
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: {  
			title, 
			cell1,
			cell2,
			toggle1,
			toggle2
		 }, setAttributes} = props;
		return (
			<div className="compare__table-row">
				<div className="compare__title-cell">  
					{ title ? title : ''}
				</div>
				<div className="compare__table-content">
						<div className="compare__row-cell">
						{ cell1 ? cell1 : 
							toggle1 ? 
							<span className="true"></span> :
							<span className="false"></span>
						}		
					</div>	
					<div className="compare__row-cell">
						{ cell2 ? cell2 : 
							toggle2 ? 
							<span className="true"></span> :
							<span className="false"></span>
						}		
					</div>	
				</div>				
			</div>			
		);
	}
} );
