/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
// import { media } from '@wordpress/icons/build-types';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/table-block-6', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Compare table block 6 companies' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
		headtitle: {
			type: 'string'
		}, 
		media1: {
			type: 'object',
			default: {}
		},
		media2: {
			type: 'object',
			default: {}
		},
		media3: {
			type: 'object',
			default: {}
		},
		media4: {
			type: 'object',
			default: {}
		},
		media5: {
			type: 'object',
			default: {}
		},
		media6: {
			type: 'object',
			default: {}
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, headtitle, media1, media2, media3, media4, media5, media6 }} = props;
        
		const TEMPLATE = [
			[ 'trilogo/table-subtitle-6', {}], 
			[ 'trilogo/table-row-6', {}], 
		];
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeHeadtitle = ( newHeadtitle ) => {
            props.setAttributes( { headtitle: newHeadtitle } );
		};
		var onSelectImage1 = function (media1) {
			props.setAttributes({ media1: media1 })
		};
		var onSelectImage2 = function (media2) {
			props.setAttributes({ media2: media2 })
		};
		var onSelectImage3 = function (media3) {
			props.setAttributes({ media3: media3 })
		};
		var onSelectImage4 = function (media4) {
			props.setAttributes({ media4: media4 })
		};
		var onSelectImage5 = function (media5) {
			props.setAttributes({ media5: media5 })
		};
		var onSelectImage6 = function (media6) {
			props.setAttributes({ media6: media6 })
		};
		return (
            <div className="compare compare__six-columns">
                <div className="container">
                    <RichText
						tagName="span"	
						className="page-title"	
						placeholder={ __('Заголовок блока') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
					<div className="compare__table-head">
						<TextControl
							tagName="span"	
							className="off-tip"	
							placeholder={ __('Заголовок колонки') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeHeadtitle}
							value={ headtitle }/>
						<div className={ media1.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media1.id ? { Image: 'url(' + media1.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage1 }
								type='image'
								value={ media1.id} 
								render={ function(obj) {
									return <Button
									className={ media1.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media1.id ? __('Upload Image')  : <img src={ media1.url } alt={ media1.alt } title={ media1.title } /> }
									</Button>
									}
								}/>
						</div>
						<div className={ media2.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media2.id ? { Image: 'url(' + media2.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage2 }
								type='image'
								value={ media2.id} 
								render={ function(obj) {
									return <Button
									className={ media2.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media2.id ? __('Upload Image')  : <img src={ media2.url } alt={ media2.alt } /> }
									</Button>
									}
								}/>
						</div>
						<div className={ media3.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media3.id ? { Image: 'url(' + media3.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage3 }
								type='image'
								value={ media3.id} 
								render={ function(obj) {
									return <Button
									className={ media3.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media3.id ? __('Upload Image')  : <img src={ media3.url } alt={ media3.alt } /> }
									</Button>
									}
								}/>
						</div>
						<div className={ media4.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media4.id ? { Image: 'url(' + media4.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage4 }
								type='image'
								value={ media4.id} 
								render={ function(obj) {
									return <Button
									className={ media4.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media4.id ? __('Upload Image')  : <img src={ media4.url } alt={ media4.alt } /> }
									</Button>
									}
								}/>
						</div>
						<div className={ media5.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media5.id ? { Image: 'url(' + media5.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage5 }
								type='image'
								value={ media5.id} 
								render={ function(obj) {
									return <Button
									className={ media5.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media5.id ? __('Upload Image')  : <img src={ media5.url } alt={ media5.alt } /> }
									</Button>
									}
								}/>
						</div>
						<div className={ media6.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
							style={ media6.id ? { Image: 'url(' + media6.url + ')' } : {} }>
								<MediaUpload
								onSelect={ onSelectImage6 }
								type='image'
								value={ media6.id} 
								render={ function(obj) {
									return <Button
									className={ media6.id ? 'image-button' : 'button button-large' }
									onClick={ obj.open }>
										{ !media6.id ? __('Upload Image')  : <img src={ media6.url } alt={ media6.alt } /> }
									</Button>
									}
								}/>
						</div>
					</div>
                    <InnerBlocks 
                        allowedBlocks={ [
							'trilogo/table-subtitle-6',
							'trilogo/table-row-6'
						] }
                        template={ TEMPLATE } />                 
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, headtitle, media1, media2, media3, media4, media5, media6 }} = props;
		return (
            <div className="compare compare__six-columns">
                <div className="container">                    
					<div className="compare__title">
						<RichText.Content 
							tagName='span' 
							className="page-title"
							value={ title } />
					</div>
					<div className="compare__content">
						<div className="compare__table-head">
							<div className="compare__title-cell">  
								{ headtitle ? headtitle : ''}
							</div>	
							<div className="compare__table-content">
								<div className="compare__row-cell">
									{ media1.id ?
									<img src={ media1.url } alt={ media1.alt ?  media1.alt : media1.title } className="compare-logo"/> : ''}
								</div>	
								<div className="compare__row-cell">
									{ media2.id ?
									<img src={ media2.url } alt={ media2.alt ?  media2.alt : media2.title } className="compare-logo"/> : ''}
								</div>	
								<div className="compare__row-cell">
									{ media3.id ?
									<img src={ media3.url } alt={ media3.alt ?  media3.alt : media3.title } className="compare-logo"/> : ''}
								</div>	
								<div className="compare__row-cell">
									{ media4.id ?
									<img src={ media4.url } alt={ media4.alt ?  media4.alt : media4.title } className="compare-logo"/> : ''}
								</div>	
								<div className="compare__row-cell">
									{ media5.id ?
									<img src={ media5.url } alt={ media5.alt ?  media5.alt : media5.title } className="compare-logo"/> : ''}
								</div>	
								<div className="compare__row-cell">
									{ media6.id ?
									<img src={ media6.url } alt={ media6.alt ?  media6.alt : media6.title} className="compare-logo"/> : ''}
								</div>
							</div>			
						</div>
						<InnerBlocks.Content />
                    </div>
                </div>
            </div>
		);
	}
} );
