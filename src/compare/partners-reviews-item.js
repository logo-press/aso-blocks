/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
// import { media } from '@wordpress/icons/build-types';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/partners-reviews-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Partner review' ), // Block title.
    parent: ['trilogo/partners-reviews-block'],
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
        media: {
            type: 'object',
			default: {}
        },
        review: {
            type: 'array',
			source: 'children',
			selector: 'p'
        },
        name: {
            typ: 'string'
        },
        pos: {
            type: 'string'
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, media, review, name, pos }} = props;

        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
        const onChangeReview = ( newReview ) => {
            props.setAttributes( { review: newReview } );
		};
        const onChangeName = ( newName ) => {
            props.setAttributes( { name: newName } );
		};
        const onChangePos = ( newPos ) => {
            props.setAttributes( { pos: newPos } );
		};
        var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (
            <div className="partners-reviews__item">
                <TextControl
                    tagName="span"	
                    className="page-title"	
                    placeholder={ __('Название компании') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>
                <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                    style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                        <MediaUpload
                        onSelect={ onSelectImage }
                        type='image'
                        value={ media.id} 
                        render={ function(obj) {
                            return <Button
                            className={ media.id ? 'image-button' : 'button button-large' }
                            onClick={ obj.open }>
                                { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt } title={ media.title } /> }
                            </Button>
                            }
                        }/>
                </div>
                <RichText
                    tagName="span"	
                    // className="page-title"	
                    placeholder={ __('Отзыв') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeReview}
                    value={ review }/>
                <TextControl
                    tagName="span"	
                    // className="off-tip"	
                    placeholder={ __('Имя автора') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeName}
                    value={ name }/>
                <TextControl
                    tagName="span"	
                    // className="off-tip"	
                    placeholder={ __('Должность автора') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangePos}
                    value={ pos }/>               			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, media, review, name, pos }} = props;
		return (
            <div className="partners-reviews__item">
                <div className="partners-reviews__company"> 
                    { media.id ?
                    <div className="company__logo">
                    <img src={ media.url } alt={ media.title }/>
                    </div> : '' }
                    <span className="company__name">{ title }</span>					
                </div>
                <div className="partners-reviews__review">
                    { review ?
                    <RichText.Content 
                        tagName='p' 
                        className="partners-reviews__review-content"
                        value={ review } />	 : '' }		                    
                </div>
                { name ?		
					<div className="partners-reviews__person">
						<span className="partners-reviews__person-name">{ name }</span>
						<span className="partners-reviews__person-position">{ pos }</span>
                    </div> : '' }
            </div>
		);
	}
} );
