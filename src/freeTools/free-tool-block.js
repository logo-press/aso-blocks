/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const IconButton = wp.components.IconButton;

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';



/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/free-tool-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Free Tool' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		whiteURL: {
			type: 'url'
        },
		whiteName: {
			type: 'string'
        },
		whiteTRG: {
			type: 'boolean',
			default: true
        },
        media: {
            type: 'object',
            default: {}
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: {  whiteURL,  whiteName, whiteTRG,  media }, setAttributes } = props;

		const onChangeWhiteURL = ( newWhiteURL ) => {
            props.setAttributes( { whiteURL: newWhiteURL } );
		};

		const onChangeWhiteName = ( newWhiteName ) => {
            props.setAttributes( { whiteName: newWhiteName } );
		};
        var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (
			<div  className="free-tool">
				<div className="container">            	
					<div className="image-block">
                        <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                            style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                                <MediaUpload
                                onSelect={ onSelectImage }
                                type='image'
                                value={ media.id} 
                                render={ function(obj) {
                                    return <Button
                                    className={ media.id ? 'image-button' : 'button button-large' }
                                    onClick={ obj.open }>
                                        { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                                    </Button>
                                    }
                                }/>
                        </div>
					</div>
					<div className="buttons">
						<div className="url-group">
							<IconButton icon="admin-links"
										className="link-btn"></IconButton>
							<TextControl
								tagName="div"
								className="link-url"
								placeholder={ __('Ссылка на страницу') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeWhiteURL}
								value={ whiteURL }/>
							<ToggleControl
								checked={ whiteTRG }
								className="link-url"
								onChange={(newWhiteTRG) => setAttributes({ whiteTRG: newWhiteTRG})} />
						</div>
						<TextControl
							tagName="span"
							className="link-name white-link"
							placeholder={ __('Бесплатные') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeWhiteName}
							value={ whiteName }/>
					</div>
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: {  whiteURL, whiteName, whiteTRG, media }} = props;
		return (
			<div className="free-tool">
				<div className="container">	
					<div className="free-tool__wrap">
				                        { media.id ?
                        <div className="free-tool__image">
                            <img src={ media.url } alt={ media.alt ? media.alt : "Banner" }/>
                        </div> : ''}
						{ whiteName && whiteURL && !whiteTRG ?
								<a href={ whiteURL } className="white-link">{ whiteName } <span className="try-free-mobile"><i
									className="ic-arrow-right-blue"></i></span></a> :
								whiteName && whiteURL && whiteTRG?
									<a href={ whiteURL } className="white-link" target="_blank" rel="noopener noreferrer">
										{ whiteName } <span className="try-free-mobile"><i
										className="ic-arrow-right-blue"></i></span></a> : ''}
										</div>
				</div>
			</div>
		);
	}
} );
