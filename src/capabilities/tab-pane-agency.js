/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const MediaUpload = wp.blockEditor.MediaUpload;
const TextControl = wp.components.TextControl;
const Button = wp.components.Button;
const el = wp.element.createElement;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/tab-pane-agency', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Content element Agency page' ), // Block title.
	parent: ['trilogo/cap-tab-content'],
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
		title: {
			type: 'string',
		}
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, title }} = props;
        
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};

		return (
            <div className="tab-pane">
				<div className="images">
					<div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
						style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
							<MediaUpload
							label="Screen"
							onSelect={ onSelectImage }
							type='image'
							value={ media.id} 
							render={ function(obj) {
								return <Button
								className={ media.id ? 'image-button' : 'button button-large' }
								onClick={ obj.open }>
									{ !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
								</Button>
								}
							}/>
					</div>
				</div>
                <TextControl
					tagName="span"		
					className="pic-name"
                    placeholder={ __('Введите название изображения') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>            			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { media, title }} = props;
		return (
            <div  className="tab-pane tab-pane__agency">
                <div className="tab-pane__title">{ title }</div>
				{ media.id ?
				<div className="tab-pane__screen">
					<img src={ media.url } alt={ media.alt ? media.id : 'some image' } className="tab-pane__big-screen"/>
				</div> : ''}
            </div>
		);
	}
} );
