/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';


const el = wp.element.createElement;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/service-application-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Compare services page Application' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
		tip1: {
			type: 'string'
        },
        tip2: {
			type: 'string'
        },
        tip3: {
			type: 'string'
        },
        tip4: {
			type: 'string'
        },
        registerURL: {
			type: 'url'
        },
        registerName: {
			type: 'string'
		},
        registerTRG: {
			type: 'boolean',
			default: true
		},
        registerModal: {
			type: 'boolean',
			default: false
		},
        demoName: {
			type: 'string'
        },
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        },
        rateURL: {
			type: 'url'
        },
        rateName: {
			type: 'string'
        },
        rateTRG: {
			type: 'boolean',
			default: true
        },
        rateModal: {
			type: 'boolean',
			default: false
        }

	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const { attributes: { 
			media, 
			title, 
			tip1, 
			tip2, 
			tip3, 
			tip4, 
			registerURL, 
			registerName, 
			registerTRG, 
			registerModal, 
			demoName, 
			greenURL, 
			greenName, 
			greenTRG, 
			greenModal, 
            rateURL,
            rateName,
            rateTRG,
            rateModal
		}, setAttributes } = props;
        
        const TEMPLATE = [[ 'core/heading', {}]];

		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip1 = ( newTip1 ) => {
            props.setAttributes( { tip1: newTip1 } );
		};
        const onChangeTip2 = ( newTip2 ) => {
            props.setAttributes( { tip2: newTip2 } );
		};
        const onChangeTip3 = ( newTip3 ) => {
            props.setAttributes( { tip3: newTip3 } );
		};
        const onChangeTip4 = ( newTip4 ) => {
            props.setAttributes( { tip4: newTip4 } );
		};
		const onChangeRegisterURL = ( newRegisterURL ) => {
            props.setAttributes( { registerURL: newRegisterURL } );
		};
		const onChangeRegisterName = ( newRegisterName ) => {
            props.setAttributes( { registerName: newRegisterName } );
		};
		const onChangeDemoURL = ( newDemoURL ) => {
            props.setAttributes( { demoURL: newDemoURL } );
		};
		const onChangeDemoName = ( newDemoName ) => {
            props.setAttributes( { demoName: newDemoName } );
		};
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};
        const onChangeRateURL = ( newRateURL ) => {
            props.setAttributes( { rateURL: newRateURL } );
		};
		const onChangeRateName = ( newRateName ) => {
            props.setAttributes( { rateName: newRateName } );
		};


		return (

            <div className="offer offer_4">
				<div className="container">
					<div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
						style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
							<MediaUpload
							onSelect={ onSelectImage }
							type='image'
							value={ media.id} 
							render={ function(obj) {
								return <Button
								className={ media.id ? 'image-button' : 'button button-large' }
								onClick={ obj.open }>
									{ !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
								</Button>
								}
							}/>
					</div>
                    <RichText
						tagName="span"	
						className="page-title"	
						placeholder={ __('Попробовать бесплатно') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
                    
					<div className="butns butns_4">
						<div className="blue">
                            {/* <TextControl
                                tagName="span"		
                                className="off-tip"	
                                placeholder={ __('Предложение') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeTip1}
                                value={ tip1 }/> */}
							<ToggleControl 
								checked={ registerModal }
								className="link-url"
								onChange={(newRegisterModal) => setAttributes({ registerModal: newRegisterModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="span"		
									className="link-url"			
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeRegisterURL}
									value={ registerURL }/>
								<ToggleControl 
									checked={ registerTRG }
									className="link-url"
									onChange={(newRegisterTRG) => setAttributes({ registerTRG: newRegisterTRG})} />
							</div>
							<TextControl
								tagName="span"		
								className="link-name register"			
								placeholder={ __('Зарегистрироваться') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeRegisterName}
								value={ registerName }/>							
						</div>
						<div className="white">
                            {/* <TextControl
                                tagName="span"		
                                className="off-tip"	
                                placeholder={ __('Предложение') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeTip2}
                                value={ tip2 }/> */}
							<TextControl
								tagName="span"		
								className="link-name btn-white"			
								placeholder={ __('Запланировать демо') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeDemoName}
								value={ demoName }/>
						</div>
                        <div className="blue">
                            {/* <TextControl
                                tagName="span"		
                                className="off-tip"	
                                placeholder={ __('Предложение') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeTip3}
                                value={ tip3 }/> */}
							<ToggleControl 
								checked={ rateModal }
								className="link-url"
								onChange={(newRateModal) => setAttributes({ rateModal: newRateModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="span"		
									className="link-url"			
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeRateURL}
									value={ rateURL }/>
								<ToggleControl 
									checked={ rateTRG }
									className="link-url"
									onChange={(newRateTRG) => setAttributes({ rateTRG: newRateTRG})} />
							</div>
							<TextControl
								tagName="span"		
								className="link-name register"			
								placeholder={ __('Зарегистрироваться') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeRateName}
								value={ rateName }/>							
						</div>
						<div className="green">
                            {/* <TextControl
                                tagName="span"		
                                className="off-tip"	
                                placeholder={ __('Предложение') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeTip4}
                                value={ tip4 }/> */}
							<ToggleControl 
								label="Modal window"
								checked={ greenModal }
								className="link-url"
								onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>							
								<TextControl
									tagName="span"		
									className="link-url green-link"
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeGreenURL}
									value={ greenURL }/>
								<ToggleControl 
									checked={ greenTRG }
									className="link-url"
									onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
							</div>	
							<TextControl
								tagName="span"		
								className="link-name green-name"			
								placeholder={ __('Посмотреть тарифы') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeGreenName}
								value={ greenName }/>	
						</div>  
					</div>			           		
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { 
			media, 
			title, 
			tip1, 
			tip2, 
			tip3, 
			tip4, 
			registerURL, 
			registerName, 
			registerTRG, 
			registerModal, 
			demoName, 
			greenURL, 
			greenName, 
			greenTRG, 
			greenModal, 
            rateURL,
            rateName,
            rateTRG,
            rateModal
            
		}} = props;
		return (
			<div className="offer offer_service-page">
				<div className="container">
					{ media.id ?
					<div className="offer__img">
						<img src={ media.url } alt={ media.alt ? media.alt : 'Offer image'}/>
					</div> : ''}
					<div className="offer__title">
						<RichText.Content 
							tagName='span' 
							value={ title } />
					</div>
					
					<div className="offer__bottom">
                        <div className="bottom-btn">
                            { tip1 ?
                            <div className="bottom-subtitle">
                                <span>{ tip1 }</span>
                            </div> : ''}
                            { registerModal && registerName ?
							<button data-micromodal-trigger="modal-1" className="register blue__link btn-large btn-blue banner__link_blue">{ registerName }</button> :
							registerName && registerURL && !registerTRG ?                        
                            <a href={ registerURL } className="register blue__link btn-large btn-blue banner__link_blue">{ registerName }</a> : 
							registerName && registerURL && registerTRG ?
                            <a href={ registerURL } className="register blue__link btn-large btn-blue banner__link_blue"  target="_blank" rel="noopener noreferrer">{ registerName }</a> : ''}
						</div>
                        <div className="bottom-btn">
                            { tip2 ?
                            <div className="bottom-subtitle">
                                <span>{ tip2 }</span>
                            </div> : ''}
                            { demoName  ?                        
                            <button data-micromodal-trigger="modal-3" className="btn btn-white">
							<object type="application/x-shockwave-flash" data="/wp-content/themes/aso/images/dest/calendar.svg" width="37" height="36">
								<img src="/wp-content/themes/aso/images/dest/calendar_2.png" alt="Calendar"/>
							</object>
							{ demoName }</button> : ''}
                        </div> 
                        <div className="bottom-btn">
                            { tip3 ?
                            <div className="bottom-subtitle">
                                <span>{ tip3 }</span>
                            </div> : ''}
                            { rateModal && rateName ?
							<button data-micromodal-trigger="modal-1" className="blue-link">{ rateName }</button> :
							rateName && rateURL && !rateTRG ?                             
                            <a href={ rateURL } className="blue-link">{ rateName }</a> :
							rateName && rateURL && rateTRG ? 
                            <a href={ greenURL } className="blue-link"  target="_blank" rel="noopener noreferrer">{ rateName }</a> : ''}
                        </div> 
                        <div className="bottom-btn">
                            { tip4 ?
                            <div className="bottom-subtitle">
                                <span>{ tip4 }</span>
                            </div> : ''}
                            { greenModal && greenName ?
						<button data-micromodal-trigger="modal-1" className="green-link">{ greenName }</button> :
							greenName && greenURL && !greenTRG ? 
                            <a href={ greenURL } className="green-link">{ greenName }</a>: ''}
                            { greenName && greenURL && greenTRG ? 
                            <a href={ greenURL } className="green-link"  target="_blank" rel="noopener noreferrer">{ greenName }</a> : ''}
                        </div> 
					</div>
				</div>
            </div>	
		);
	}
} );
