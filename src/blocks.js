/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './about/about-block.js';
import './about/about-item.js';
import './about/figures-block.js';
import './about/figures-item.js';
import './about/team-block.js';
import './about/team-item.js';
import './about/projects-block.js';
import './about/projects-item.js';

import './about/contact-block.js';
import './about/persons.js';
import './about/person.js';
import './about/quick-connection.js';
import './about/offices.js';
import './about/office.js';

import './agency/service-block.js';
import './agency/service-items.js';
import './agency/service-item.js';
import './agency/service-bottom.js';
import './agency/service-top.js';
import './agency/service-side.js';
import './agency/service-side-item.js';

import './agency/agency-form-block.js';
import './agency/agency-desc-block.js';



import './app/application-block.js';
import './app/aso-application-block.js';
import './app/service-application-block.js';

import './appointment/appointment-block.js';
import './appointment/app-item.js';

import './banner/aso-banner-block.js';
import './banner/about-banner-block.js';
import './banner/rates-banner-block.js';
import './banner/agency-banner-block.js';
import './banner/main-banner-block.js';
import './banner/main-banner-blockv2.js';
import './banner/main-banner-block-eng.js';
import './banner/icon-1.js';
import './banner/icon-2.js';
import './banner/icon-3.js';
import './banner/icon-4.js';
import './banner/icon-5.js';
import './banner/icon-6.js';
import './banner/icon-7.js';

import './freeTools/free-tool-block.js';

import './capabilities/capabilities-block.js';
import './capabilities/capabilities-agency-block.js';
import './capabilities/cap-item.js';
import './capabilities/cap-nav-tab.js';
import './capabilities/cap-nav-tab-agency.js';
import './capabilities/cap-nav-item.js';
import './capabilities/cap-tab-content.js';
import './capabilities/tab-pane.js';
import './capabilities/tab-panev2.js';
import './capabilities/tab-pane-agency.js';


import './comparing/comparing-rates-block.js';
import './comparing/rate-slide.js';
import './comparing/rate-slide-hot.js';
import './comparing/rate-slide-item.js';


import './compare/load-report.js';
import './compare/table-block-2.js';
import './compare/table-block-6.js';
import './compare/table-subtitle-2.js';
import './compare/table-subtitle-6.js';
import './compare/table-row-2.js';
import './compare/table-row-6.js';
import './compare/competitor-block.js';
import './compare/competitor-item.js';

import './compare/partners-reviews-block.js';
import './compare/partners-reviews-item.js';

import './details/details-block.js';
import './details/detail-grey-r.js';
import './details/detail-grey.js';
import './details/detail-white-r.js';
import './details/detail-white.js';
import './details/detail-yellow-r.js';
import './details/detail-yellow.js';
import './details/detail-soon-r.js';
import './details/detail-soon.js';
import './details/detail-grey-rv2.js';
import './details/detail-greyv2.js';
import './details/detail-white-rv2.js';
import './details/detail-whitev2.js';
import './details/detail-yellow-rv2.js';
import './details/detail-yellowv2.js';
import './details/detail-soon-rv2.js';
import './details/detail-soonv2.js';

import './faq/faq-block.js';
import './faq/rates-faq-block.js';
import './faq/faq-item.js';

import './partners/partners-block.js';
import './partners/partners-agency-block.js';
import './partners/partners-eng-block.js';
import './partners/partners-services-block.js';
import './partners/partners-slider.js';
import './partners/partners-slide.js';
import './partners/partners-reviews.js';
import './partners/partners-review.js';

import './rates/rates-block.js';

import './free-main/free-main-page.js';
import './free-main/basic-item.js';

import './rates/basic.js';
import './rates/basic-item.js';
import './rates/personal.js';
import './rates/rate-nav.js';
import './rates/rate-nav-item.js';
import './rates/rates-container.js';
import './rates/rate.js';
import './rates/rate-item.js';
import './rates/rate-pack.js';
import './rates/rate-el.js';
import './rates/rate-hot.js';
import './rates/rate-purple.js';

import './tasks/tasks-block.js';
import './tasks/tasks-agency-block.js';
import './tasks/tasks-item.js';
import './tasks/tasks-agency-item.js';

import './tools/tools-block.js';
import './tools/tools-services-block.js';
import './tools/tool.js';
import './tools/tool-services.js';

import './trial/male-account.js';
import './trial/trial.js';



import './custom-jquery.js';

var $ = jQuery;

$(document).ready(function () {
    $(document).on('click', '.link-btn', function (e) {
        var parent = $(this).parent('.url-group');
        var link = parent.find('.link-url');
        if(link.hasClass('shown')){
            link.removeClass('shown')
        } else {
            link.addClass('shown');
        }
    });
});