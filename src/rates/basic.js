/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/basic', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __( 'TriloGO Basic' ), // Block title.
    parent: ['trilogo/rates-block'],
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
		tip: {
			type: 'string'
        },
		tip2: {
			type: 'array',
			source: 'children',
			selector: 'p.rate-el__pos'
		},
        ribbon: {
			type: 'string'
        },
        buyURL: {
			type: 'url'
        },
        buyName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, tip, tip2, ribbon, buyURL, buyName, greenTRG, greenModal }, setAttributes} = props;
        const TEMPLATE = [
			[ 'trilogo/basic-item', {}]];
		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
		const onChangeTip2 = ( newTip2 ) => {
			props.setAttributes( { tip2: newTip2 } );
		};
		const onChangeRibbon = ( newRibbon ) => {
            props.setAttributes( { ribbon: newRibbon } );
		};
		const onChangeBuyURL = ( newBuyURL ) => {
            props.setAttributes( { buyURL: newBuyURL } );
		};
		const onChangeBuyName = ( newBuyName ) => {
            props.setAttributes( { buyName: newBuyName } );
		};
		return (
            <div  className="tabs__basic">
                <RichText
                    tagName="span"	
                    className="block-title"
                    placeholder={ __('Введите название предложения') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>
				<TextControl
                    tagName="span"		
                    className="tip"
                    placeholder={ __('Доступно во всех тарифах') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTip}
                    value={ tip }/>
				<RichText
					tagName="p"
					className="rate-cont"
					placeholder={ __('Содержание тарифа') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangeTip2}
					value={ tip2 }/>
                <TextControl
                    tagName="span"	
                    className="ribbon"	
                    placeholder={ __('Надпись на уголке') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeRibbon}
                    value={ ribbon }/>                 
                <InnerBlocks 
                        allowedBlocks={ ['trilogo/basic-item'] }
                        template={ TEMPLATE } />         
                <div className="buttons">
                    <ToggleControl 
							label="Modal window"
							checked={ greenModal }
							className="link-url"
							onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
                    <div className="url-group">
                        <IconButton icon="admin-links"
                            className="link-btn"></IconButton> 
                        <TextControl
                            tagName="span"
                            className="link-url try-link"		
                            placeholder={ __('Укажите ссылку на страницу') }
                            keepPlaceholderOnFocus= {true}
                            onChange= {onChangeBuyURL}
                            value={ buyURL }/>
                        <ToggleControl 
                            checked={ greenTRG }
                            className="link-url"
                            onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
                    </div>
                    <TextControl
                        tagName="span"	
                        className="link-name try-name"	
                        placeholder={ __('Попробовать') }
                        keepPlaceholderOnFocus= {true}
                        onChange= {onChangeBuyName}
                        value={ buyName }/>			
                </div>
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { title, tip, tip2, ribbon, buyURL, buyName, greenTRG, greenModal }} = props;
		return (
            <div className="tabs__basic">
                { ribbon ?
                <div className="basic__corner-ribbon">{ ribbon }</div> : ''} 
                <div className="basic__title">
                    <RichText.Content 
                        tagName='span' 
                        className='basic-title'
                        value={ title } />
                    { tip ?
                    <span className="basic__tip">{ tip }</span> : ''} 
                </div>
				{ tip2 ?
					<RichText.Content
						tagName='p'
						className="rate-el__pos"
						value={ tip2 } /> : '' }
                <ul className="basic__pos-list">
                    <InnerBlocks.Content />
                </ul>
                { buyName  ?
                <div className="basic__button">
					{ greenModal ?
					<button data-micromodal-trigger="modal-1" className="blue-link">{ greenName }</button> :
					buyURL && !greenTRG ?
                    <a href={ buyURL } className="blue-link">{ buyName }</a> :
					buyURL && greenTRG ?
                    <a href={ buyURL } className="blue-link"  target="_blank" rel="noopener noreferrer">{ buyName }</a> : ''} 	
                </div> : ''} 
            </div>
		);
	}
} );
