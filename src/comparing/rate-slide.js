/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/rate-slide', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Rate slide' ), // Block title.
    parent: ['trilogo/comparing-rates-block'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
		},
		tip: {
			type: 'string'
        },
        price: {
			type: 'string'
        },
        buyURL: {
			type: 'url'
        },
        buyName: {
			type: 'string'
        },
        buyTRG: {
			type: 'boolean',
			default: true
        },
        buyModal: {
			type: 'boolean',
			default: false
        },
        yearPrice: {
			type: 'string'
        },
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, tip, price, buyURL, buyName, yearPrice, greenTRG, buyModal }, setAttributes} = props;
        
        const TEMPLATE = [[ 'core/list', {}]];
		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
        };
        const onChangePrice = ( newPrice ) => {
            props.setAttributes( { price: newPrice } );
		};
		const onChangeBuyURL = ( newBuyURL ) => {
            props.setAttributes( { buyURL: newBuyURL } );
		};
		const onChangeBuyName = ( newBuyName ) => {
            props.setAttributes( { buyName: newBuyName } );
		};
		const onChangeYearPrice = ( newYearPrice ) => {
            props.setAttributes( { yearPrice: newYearPrice } );
		};
		return (

            <div className="comparing-rates__item">
				<div className="title-wrapper">
					<RichText
						tagName="span"
						className="block-title"	
						placeholder={ __('Название тарифа') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
				</div>				
				<RichText
					tagName="span"	
					className="rate-cont"	
					placeholder={ __('Содержание тарифа') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangeTip}
					value={ tip }/>
				<TextControl
					tagName="span"		
					className="rate-pr"
					placeholder={ __('Введите цену') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangePrice}
					value={ price }/>
				<div className="list-inner-elements">
					<InnerBlocks 
						allowedBlocks={ ['core/list'] }
						template={ TEMPLATE } />
				</div>
				<div className="buttons">
					<ToggleControl 
						label="Modal window"
						checked={ buyModal }
						className="link-url"
						onChange={(newBuyModal) => setAttributes({ buyModal: newBuyModal})} />
					<div className="url-group">
						<IconButton icon="admin-links"
							className="link-btn"></IconButton>		
						<TextControl
							tagName="span"	
							className="link-url buy-link"	
							placeholder={ __('Укажите ссылку на тариф') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeBuyURL}
							value={ buyURL }/>
						<ToggleControl 
							checked={ greenTRG }
							className="link-url"
							onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
					</div>		
					<TextControl
						tagName="span"
						className="link-name buy-name"		
						placeholder={ __('Купить за ...') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeBuyName}
						value={buyName }/>	
				</div>		
				<TextControl
					tagName="span"
					className="year-pr"		
					placeholder={ __('Цена за год') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangeYearPrice}
					value={ yearPrice }/>
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, tip, price, buyURL, buyName, yearPrice, greenTRG, buyModal }, setAttributes} = props;
		return (
			<div className="comparing-rates__item">
                <div className="comparing-rates__item-title">
                    <RichText.Content 
						tagName='span' 
						className="comparing-rates__item-header"
						value={ title } />
					{ tip ?
					<RichText.Content 
						tagName='span' 
						className="comparing-rates__item-pos"
                        value={ tip } /> : '' }
                    {/* <span className="comparing-rates__item-pos">{ tip }</span> */}
                </div>
				{ price ?
                <div className="comparing-rates__item-prices">
                    <span className="comparing-rates__item-price">{ price }</span>
                </div> : ''}
					<InnerBlocks.Content />
				{ buyName ? 
				<div className="comparing-rates__item-button">
				{ buyModal ?
				<button data-micromodal-trigger="modal-1" className="blue-link">{ buyName }</button> :
				buyURL && !greenTRG ?                
				<a href={ buyURL } className="blue-link">{ buyName }</a> : 
				buyURL && greenTRG ?                
				<a href={ buyURL } className="blue-link" target="_blank" rel="noopener noreferrer">{ buyName }</a> : '' }
                </div> : ''}
                <span className="comparing-rates__item-price-year">{ yearPrice }</span>
            </div>
		);
	}
} );
