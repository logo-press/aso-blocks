/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

const MediaUpload = wp.blockEditor.MediaUpload;
const TextControl = wp.components.TextControl;
const RichText = wp.blockEditor.RichText;
const Button = wp.components.Button;
const el = wp.element.createElement;
const IconButton = wp.components.IconButton;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/office', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Office' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
		addr: {
			type: 'string'
		}
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, addr }} = props;
        
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		const onChangeAddr = ( newAddr ) => {
            props.setAttributes( { addr: newAddr } );
		};

		return (
            <div className="contacts__office">
				<div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                    style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                        <MediaUpload
                        onSelect={ onSelectImage }
                        type='image'
                        value={ media.id} 
                        render={ function(obj) {
                            return <Button
                            className={ media.id ? 'image-button' : 'button button-large' }
                            onClick={ obj.open }>
                                { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                            </Button>
                            }
                        }/>
                </div>
				<TextControl
					tagName="span"	
					className="department"	
					placeholder={ __('Адрес отделения') }
					keepPlaceholderOnFocus= {true}
					onChange= {onChangeAddr}
					value={ addr }/>	
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { media, addr }} = props;
		return (
			<div  className="contacts__office">
				{ media.id  ?
				<div className="office__flag">
					<img src={ media.url } alt={ media.alt ? media.alt : 'some image' }/>
				</div> : ''}
				<div className="office__address">
					<p>{ addr }</p>
				</div>
            </div>	
		);
	}
} );
