/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/person', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Person' ), // Block title.
	parent: ['trilogo/persons'],
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
		dep: {
			type: 'string'
		},
		pos: {
			type: 'string'
        },
        telega: {
			type: 'string'
        },
        telegaTRG: {
			type: 'boolean',
			default: true
        },
        email: {
			type: 'string'
        },
        linked: {
			type: 'string'
        },
        linkedTRG: {
			type: 'boolean',
			default: true
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, dep, pos, telega, email, linked, telegaTRG, linkedTRG }, setAttributes} = props;
        
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		const onChangeDep = ( newDep ) => {
            props.setAttributes( { dep: newDep } );
		};
		const onChangePos = ( newPos ) => {
            props.setAttributes( { pos: newPos } );
		};
		const onChangeTelega = ( newTelega ) => {
            props.setAttributes( { telega: newTelega } );
		};
		const onChangeEmail = ( newEmail ) => {
            props.setAttributes( { email: newEmail } );
		};
		const onChangeLinked = ( newLinked ) => {
            props.setAttributes( { linked: newLinked } );
		};

		return (
            <div className="contacts__person">
				<div className="container">
					<div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
						style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
							<MediaUpload
							onSelect={ onSelectImage }
							type='image'
							value={ media.id} 
							render={ function(obj) {
								return <Button
								className={ media.id ? 'image-button' : 'button button-large' }
								onClick={ obj.open }>
									{ !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
								</Button>
								}
							}/>
					</div>
                    <TextControl
						tagName="span"	
						className="department"	
						placeholder={ __('Отделение') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeDep}
						value={ dep }/>
					<TextControl
						tagName="span"	
						className="position"	
						placeholder={ __('Должность') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangePos}
						value={ pos }/>
					<div className="person-link">
						<TextControl
							tagName="span"	
							className="telegram"	
							placeholder={ __('Телеграм') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeTelega}
							value={ telega }/>
						<ToggleControl 
							checked={ telegaTRG }
							className="link-url"
							onChange={(newTelegaTRG) => setAttributes({ telegaTRG: newTelegaTRG})} />
					</div>
					<div className="person-link">
						<TextControl
							tagName="span"	
							className="e-mail"	
							placeholder={ __('Электронный адрес') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeEmail}
							value={ email }/>
					</div>
					<div className="person-link">
						<TextControl
							tagName="span"	
							className="linkedIn"	
							placeholder={ __('Аккаунт в LinkedIn') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeLinked}
							value={ linked }/>			             
						<ToggleControl 
							checked={ linkedTRG }
							className="link-url"
							onChange={(newLinkedTRG) => setAttributes({ linkedTRG: newLinkedTRG})} />
					</div>			
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { media, dep, pos, telega, email, linked, telegaTRG, linkedTRG }, setAttributes} = props;
		return (
			<div className="contacts__person">
				{ media.id  ?
				<div className="person__img">
					<img src={ media.url } alt={ media.alt ? media.alt : "some image" }/>
				</div> : ''}
				<div className="person__title">
					<span>{ dep }</span>
					<p>{ pos }</p>
				</div>
				<div className="person__bottom">
					{ telega && !telegaTRG ?
					<a href={ "https://telegram.im/@" + telega } className="person-link">
						<svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M4.5717 9.27069L6.93481 14.9307L10.0113 11.9831L15.286 16L19 0L0 7.58278L4.5717 9.27069ZM13.5707 4.59056L7.75513 9.67292L7.03078 12.2886L5.69267 9.08278L13.5707 4.59056Z" fill="#A6B9DB"/>
						</svg>
						{ telega }</a> : ''}
					{ telega && telegaTRG ?
					<a href={ "https://telegram.im/@" + telega } className="person-link" target="_blank" rel="noopener noreferrer">
						<svg width="19" height="16" viewBox="0 0 19 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M4.5717 9.27069L6.93481 14.9307L10.0113 11.9831L15.286 16L19 0L0 7.58278L4.5717 9.27069ZM13.5707 4.59056L7.75513 9.67292L7.03078 12.2886L5.69267 9.08278L13.5707 4.59056Z" fill="#A6B9DB"/>
						</svg>
						{ telega }</a> : ''}
					{ email ?
					<a href={ "mailto:" + email } className="person-link">
						<svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M16.2475 0H1.08256C0.542038 0 0.113087 0.402954 0.0318459 0.921811L8.66503 6.60974L17.2982 0.921811C17.217 0.402954 16.788 0 16.2475 0ZM0 9.74022V2.19675L5.84283 6.04648L0 9.74022ZM17.3306 9.74022L11.4878 6.04648L17.3306 2.19675V9.74022ZM8.96319 7.71025L10.4959 6.7007L17.2963 11.001C17.2129 11.5156 16.7861 11.9153 16.2478 11.9153H1.08284C0.544487 11.9153 0.117703 11.5156 0.0342956 11.001L6.83468 6.69961L8.36743 7.71025C8.45842 7.76983 8.56132 7.79907 8.66531 7.79907C8.7693 7.79907 8.8722 7.76983 8.96319 7.71025Z" fill="#A6B9DB"/>
						</svg>
						{ email }</a> : ''}
					{ linked && !linkedTRG ?
					<a href={ linked } className="linked-link">
						<svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M19 38C29.4934 38 38 29.4934 38 19C38 8.50659 29.4934 0 19 0C8.50659 0 0 8.50659 0 19C0 29.4934 8.50659 38 19 38Z" fill="#007AB9"/>
							<path d="M30.3525 20.5295V28.3628H25.811V21.0545C25.811 19.2195 25.1553 17.9663 23.5109 17.9663C22.2561 17.9663 21.5106 18.81 21.1814 19.6269C21.0619 19.9189 21.031 20.3243 21.031 20.7338V28.3625H16.4892C16.4892 28.3625 16.5501 15.9846 16.4892 14.7033H21.0314V16.639C21.0222 16.6542 21.0094 16.6691 21.0012 16.6837H21.0314V16.639C21.6349 15.7103 22.7113 14.3826 25.1245 14.3826C28.1124 14.3826 30.3525 16.3348 30.3525 20.5295ZM11.738 8.11914C10.1844 8.11914 9.16797 9.13894 9.16797 10.4788C9.16797 11.7902 10.1549 12.8395 11.6784 12.8395H11.7078C13.2919 12.8395 14.2768 11.7902 14.2768 10.4788C14.2467 9.13894 13.2919 8.11914 11.738 8.11914ZM9.43791 28.3628H13.9781V14.7033H9.43791V28.3628Z" fill="#F1F2F2"/>
							</svg>
						<i className="ic-arrow-right-white"></i> 
					</a> : ''}
					{ linked && linkedTRG ?
					<a href={ linked } className="linked-link"  target="_blank" rel="noopener noreferrer">
						<svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M19 38C29.4934 38 38 29.4934 38 19C38 8.50659 29.4934 0 19 0C8.50659 0 0 8.50659 0 19C0 29.4934 8.50659 38 19 38Z" fill="#007AB9"/>
							<path d="M30.3525 20.5295V28.3628H25.811V21.0545C25.811 19.2195 25.1553 17.9663 23.5109 17.9663C22.2561 17.9663 21.5106 18.81 21.1814 19.6269C21.0619 19.9189 21.031 20.3243 21.031 20.7338V28.3625H16.4892C16.4892 28.3625 16.5501 15.9846 16.4892 14.7033H21.0314V16.639C21.0222 16.6542 21.0094 16.6691 21.0012 16.6837H21.0314V16.639C21.6349 15.7103 22.7113 14.3826 25.1245 14.3826C28.1124 14.3826 30.3525 16.3348 30.3525 20.5295ZM11.738 8.11914C10.1844 8.11914 9.16797 9.13894 9.16797 10.4788C9.16797 11.7902 10.1549 12.8395 11.6784 12.8395H11.7078C13.2919 12.8395 14.2768 11.7902 14.2768 10.4788C14.2467 9.13894 13.2919 8.11914 11.738 8.11914ZM9.43791 28.3628H13.9781V14.7033H9.43791V28.3628Z" fill="#F1F2F2"/>
							</svg>
						<i className="ic-arrow-right-white"></i> 
					</a> : ''}
				</div>
            </div>	
		);
	}
} );
