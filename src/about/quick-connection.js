/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

const el = wp.element.createElement;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/quick-connection', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Quick connection' ), // Block title.
	parent: ['trilogo/contact-block'],
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'string'
		},
		phone: {
			type: 'string'
        },
        email: {
			type: 'string'
        },
        face: {
			type: 'url'
        },
        faceTRG: {
			type: 'boolean',
			default: true
        },
        twit: {
			type: 'url'
        },
        twitTRG: {
			type: 'boolean',
			default: true
        },
        link: {
			type: 'url'
		},
        linkTRG: {
			type: 'boolean',
			default: true
		},
        tele: {
			type: 'url'
		},
        teleTRG: {
			type: 'boolean',
			default: true
		},
        yout: {
			type: 'url'
		},
        youtTRG: {
			type: 'boolean',
			default: true
		},
		form: {
			type: 'string'
		}
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, phone, email, face, faceTRG, twit, twitTRG, link, linkTRG, form, tele, teleTRG, yout, youtTRG }, setAttributes} = props;

		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangePhone = ( newPhone ) => {
            props.setAttributes( { phone: newPhone } );
		};
		const onChangeEmail = ( newEmail ) => {
            props.setAttributes( { email: newEmail } );
		};
		const onChangeFace = ( newFace ) => {
            props.setAttributes( { face: newFace } );
		};
		const onChangeTwit = ( newTwit ) => {
            props.setAttributes( { twit: newTwit } );
		};
		const onChangeLink = ( newLink ) => {
            props.setAttributes( { link: newLink } );
		};
		const onChangeTele = ( newTele ) => {
            props.setAttributes( { tele: newTele } );
		};
		const onChangeYout = ( newYout ) => {
            props.setAttributes( { yout: newYout } );
		};
		const onChangeForm = ( newForm ) => {
            props.setAttributes( { form: newForm } );
		};

		return (

            <div className="contacts__connect">
				<div className="container">
                    <TextControl
						tagName="h4"	
						placeholder={ __('Быстрая связь:') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
					<div className="url-group">
						<TextControl
							tagName="span"		
							className="link-name"			
							placeholder={ __('Phone') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangePhone}
							value={ phone }/>							
						<TextControl
							tagName="span"		
							className="link-name"			
							placeholder={ __('E-mail') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeEmail}
							value={ email }/>							
					</div>
					<div className="social-links">
						<div className="target-link">
							<TextControl
								tagName="span"		
								className="link-url"			
								placeholder={ __('Facebook') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeFace}
								value={ face }/>
							<ToggleControl 
								checked={ faceTRG }
								className="link-url"
								onChange={(newFaceTRG) => setAttributes({ faceTRG: newFaceTRG})} />
						</div>
						<div className="target-link">
							<TextControl
								tagName="span"		
								className="link-url"			
								placeholder={ __('Twitter') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeTwit}
								value={ twit }/>
							<ToggleControl 
								checked={ twitTRG }
								className="link-url"
								onChange={(newTwitTRG) => setAttributes({ twitTRG: newTwitTRG})} />
						</div>
						<div className="target-link">
							<TextControl
								tagName="span"		
								className="link-url"			
								placeholder={ __('LinkedIn') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeLink}
								value={ link }/>
							<ToggleControl 
								checked={ linkTRG }
								className="link-url"
								onChange={(newLinkTRG) => setAttributes({ linkTRG: newLinkTRG})} />
						</div>
						<div className="target-link">
							<TextControl
								tagName="span"		
								className="link-url"			
								placeholder={ __('Telegram') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeTele}
								value={ tele }/>
							<ToggleControl 
								checked={ teleTRG }
								className="link-url"
								onChange={(newTeleTRG) => setAttributes({ teleTRG: newTeleTRG})} />
						</div>
						<div className="target-link">
							<TextControl
								tagName="span"		
								className="link-url"			
								placeholder={ __('YouTube') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeYout}
								value={ yout }/>
							<ToggleControl 
								checked={ youtTRG }
								className="link-url"
								onChange={(newYoutTRG) => setAttributes({ youtTRG: newYoutTRG})} />
						</div>
					</div>
					<div className="contact-form">					
						<TextControl
							tagName="span"		
							placeholder={ __('Контактная форма') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeForm}
							value={ form }/>
					</div>	
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		return (
			<div  className="contacts__connect">
				{ props.attributes.title ?			
				<h4 className="connect__title">{ props.attributes.title }</h4>: ''}
				<div className="connect__links">
					{ props.attributes.phone ?
					<div>
						<a href={ "tel:" + props.attributes.phone.replaceAll(/\D/g, "") } className="connect__link" rel="noopener noreferrer">{ props.attributes.phone }</a>
						<span> / </span>
					</div> : ''}						
					{ props.attributes.email ?
					<div>
						<a href={ "mailto:" + props.attributes.email } className="connect__link" target="_blank" rel="noopener noreferrer">{ props.attributes.email }</a>
						<span> / </span>
					</div> : ''}
					<ul className="social__list">
						{ props.attributes.face && !props.attributes.faceTRG ?
						<li className="connect__social-item"><a href={ props.attributes.face } className="social__link"><i className="ic ic-face"></i></a></li> : ''}
						{ props.attributes.face && props.attributes.faceTRG ?
						<li className="connect__social-item"><a href={ props.attributes.face } className="social__link" target="_blank" rel="noopener noreferrer"><i className="ic ic-face"></i></a></li> : ''}
						{ props.attributes.twit && !props.attributes.twitTRG ?
						<li className="connect__social-item"><a href={ props.attributes.twit } className="social__link"><i className="ic ic-tweet"></i></a></li> : ''}
						{ props.attributes.twit && props.attributes.twitTRG ?
						<li className="connect__social-item"><a href={ props.attributes.twit } className="social__link" target="_blank" rel="noopener noreferrer"><i className="ic ic-tweet"></i></a></li> : ''}
						{ props.attributes.link && !props.attributes.linkTRG ?
						<li className="connect__social-item"><a href={ props.attributes.link } className="social__link"><i className="ic ic-linkd"></i></a></li> : ''}
						{ props.attributes.link && props.attributes.linkTRG ?
						<li className="connect__social-item"><a href={ props.attributes.link } className="social__link" target="_blank" rel="noopener noreferrer"><i className="ic ic-linkd"></i></a></li> : ''}
						{ props.attributes.tele && !props.attributes.teleTRG ?
						<li className="connect__social-item"><a href={ props.attributes.tele } className="social__link"><i className="ic ic-telegram-1"></i></a></li> : ''}
						{ props.attributes.tele && props.attributes.teleTRG ?
						<li className="connect__social-item"><a href={ props.attributes.tele } className="social__link" target="_blank" rel="noopener noreferrer"><i className="ic ic-telegram-1"></i></a></li> : ''}
						{ props.attributes.yout && !props.attributes.youtTRG ?
						<li className="connect__social-item"><a href={ props.attributes.yout } className="social__link"><i className="ic ic-youtube1"></i></a></li> : ''}
						{ props.attributes.yout && props.attributes.youtTRG ?
						<li className="connect__social-item"><a href={ props.attributes.yout } className="social__link" target="_blank" rel="noopener noreferrer"><i className="ic ic-youtube1"></i></a></li> : ''}
					</ul>
				</div>
				{ props.attributes.form ?
				<div className="connect__bottom">						
					{ props.attributes.form }
				</div> : ''}
            </div>	
		);
	}
} );
