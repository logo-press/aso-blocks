/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const IconButton = wp.components.IconButton;

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';



/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/main-banner-blockv2', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Main banner with 1 image' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h1'
		},
		tip: {
			type: 'array',
			source: 'children',
			selector: 'p'
        },
        greenURL: {
			type: 'url'
        },
		whiteURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
		whiteName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
		whiteTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        },
        media: {
            type: 'object',
            default: {}
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, tip, greenURL, whiteURL,  greenName, greenTRG, whiteName, whiteTRG, greenModal, media }, setAttributes } = props;

		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeWhiteURL = ( newWhiteURL ) => {
            props.setAttributes( { whiteURL: newWhiteURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};
		const onChangeWhiteName = ( newWhiteName ) => {
            props.setAttributes( { whiteName: newWhiteName } );
		};
        var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (
			<div  className="main-banner ver2">
				<div className="container">            	
					<RichText
						tagName="h1"	
						className="page-title"				
						placeholder={ __('Введите заголовок баннера') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
					<RichText
						tagName="p"	
						className="banner__tip"
						placeholder={ __('Анализ конкурентов, создание АСО...') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTip}
						value={ tip }/>
						<div className="buttons">
							<ToggleControl 
								label="Modal window"
								checked={ greenModal }
								className="link-url"
								onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="div"		
									className="link-url"
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeGreenURL}
									value={ greenURL }/>
								<ToggleControl 
									checked={ greenTRG }
									className="link-url"
									onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
							</div>							
							<TextControl
								tagName="span"		
								className="link-name green-link"			
								placeholder={ __('Попробовать бесплатно') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeGreenName}
								value={ greenName }/>							
						</div>					
					<div className="image-block">
                        <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                            style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                                <MediaUpload
                                onSelect={ onSelectImage }
                                type='image'
                                value={ media.id} 
                                render={ function(obj) {
                                    return <Button
                                    className={ media.id ? 'image-button' : 'button button-large' }
                                    onClick={ obj.open }>
                                        { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                                    </Button>
                                    }
                                }/>
                        </div>
					</div>
					<div className="buttons">
						<div className="url-group">
							<IconButton icon="admin-links"
										className="link-btn"></IconButton>
							<TextControl
								tagName="div"
								className="link-url"
								placeholder={ __('Ссылка на страницу') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeWhiteURL}
								value={ whiteURL }/>
							<ToggleControl
								checked={ whiteTRG }
								className="link-url"
								onChange={(newWhiteTRG) => setAttributes({ whiteTRG: newWhiteTRG})} />
						</div>
						<TextControl
							tagName="span"
							className="link-name white-link"
							placeholder={ __('Смотреть ASO гайды') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeWhiteName}
							value={ whiteName }/>
					</div>
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, tip, greenURL, whiteURL, greenName, whiteName, whiteTRG, greenTRG, greenModal, media }} = props;
		return (
			<div className="main-banner-2">
				<div className="container">	
					<div className="main-banner ver2">			
						<div className="main-banner__content">
							<RichText.Content 
								tagName='h1' 
								className="main-banner__title"
								value={ title } />
							<RichText.Content 
								tagName='p' 
								className="main-banner__subtitle"
								value={ tip } />
							{ greenName && greenModal ?
							<button data-micromodal-trigger="modal-1" className="green-link">{ greenName } <span className="try-free-mobile"><i
							className="ic-arrow-right-white"></i></span></button> :
							greenName && greenURL && !greenTRG ?
							<a href={ greenURL } className="green-link">{ greenName } <span className="try-free-mobile"><i
										className="ic-arrow-right-white"></i></span></a> :
							greenName && greenURL && greenTRG?
							<a href={ greenURL } className="green-link" target="_blank" rel="noopener noreferrer">{ greenName } <span className="try-free-mobile"><i
										className="ic-arrow-right-white"></i></span></a> : ''}
						</div>
                        { media.id ?
                        <div className="main-banner__grid-image">
                            <img src={ media.url } alt={ media.alt ? media.alt : "Banner" }/>
                        </div> : ''}
						{ whiteName && whiteURL && !whiteTRG ?
								<a href={ whiteURL } className="white-link"><span><svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M23.0004 9.792C22.9708 10.1577 22.9564 10.5254 22.9087 10.8888C22.7745 11.9107 22.7233 12.9562 22.4631 13.9459C21.9952 15.725 20.7673 16.7443 18.9437 16.9999C15.5643 17.4737 12.1661 17.5556 8.76418 17.3852C7.25965 17.3099 5.75646 17.1505 4.2612 16.9615C2.52549 16.742 1.15261 15.4059 0.833276 13.6673C0.413162 11.38 0.259373 9.07209 0.466566 6.75173C0.557557 5.73271 0.662803 4.70876 0.861684 3.70699C1.21489 1.92788 2.64047 0.692272 4.43397 0.454182C7.38244 0.0627671 10.3432 -0.0504852 13.3125 0.019374C15.2558 0.0650948 17.1939 0.196034 19.1182 0.487769C20.8637 0.752413 22.2485 2.05375 22.5451 3.81011C22.741 4.97052 22.825 6.14992 22.9592 7.3208C22.9725 7.43639 22.9867 7.55187 23.0004 7.66741C23.0004 8.3756 23.0004 9.0838 23.0004 9.792ZM11.7012 16.0426C13.8944 16.0362 16.1221 15.9534 18.7468 15.5978C20.0217 15.4252 20.9579 14.5824 21.179 13.3291C21.4137 11.9989 21.5523 10.6572 21.5847 9.30589C21.6264 7.5749 21.4731 5.85862 21.1851 4.15337C20.9872 2.98171 20.1403 2.09402 18.9658 1.90175C17.0889 1.59451 15.1924 1.47968 13.2933 1.43489C10.3756 1.36607 7.4657 1.4701 4.56942 1.86654C3.42441 2.02327 2.45315 2.83734 2.246 3.98049C2.0624 4.99376 1.93551 6.02135 1.85338 7.04822C1.68555 9.14641 1.83209 11.2347 2.20086 13.3045C2.39189 14.3766 3.00739 15.1302 4.05392 15.4681C4.43428 15.5909 4.84581 15.6259 5.24626 15.6756C7.38952 15.9416 9.54235 16.0475 11.7012 16.0426Z" fill="#0063EE"/>
									<path d="M8.68338 8.72569C8.6833 7.65678 8.68199 6.58786 8.68377 5.51895C8.68492 4.83319 9.24042 4.51517 9.82944 4.86643C11.6231 5.93609 13.4158 7.00749 15.2067 8.08191C15.7718 8.42099 15.7698 9.05211 15.2051 9.38952C13.4186 10.4569 11.6297 11.5202 9.84401 12.5888C9.59377 12.7386 9.34238 12.803 9.0723 12.6642C8.80499 12.5269 8.68202 12.3003 8.68262 11.9988C8.68477 10.9078 8.68347 9.81672 8.68338 8.72569ZM10.1049 10.7835C11.2576 10.0952 12.3817 9.42403 13.5385 8.7333C12.3769 8.03803 11.2526 7.36504 10.1049 6.67807C10.1049 8.0575 10.1049 9.39705 10.1049 10.7835Z" fill="#0063EE"/>
								</svg>
								</span>{ whiteName } <span className="try-free-mobile"><i
									className="ic-arrow-right-blue"></i></span></a> :
								whiteName && whiteURL && whiteTRG?
									<a href={ whiteURL } className="white-link" target="_blank" rel="noopener noreferrer">
										<span><svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M23.0004 9.792C22.9708 10.1577 22.9564 10.5254 22.9087 10.8888C22.7745 11.9107 22.7233 12.9562 22.4631 13.9459C21.9952 15.725 20.7673 16.7443 18.9437 16.9999C15.5643 17.4737 12.1661 17.5556 8.76418 17.3852C7.25965 17.3099 5.75646 17.1505 4.2612 16.9615C2.52549 16.742 1.15261 15.4059 0.833276 13.6673C0.413162 11.38 0.259373 9.07209 0.466566 6.75173C0.557557 5.73271 0.662803 4.70876 0.861684 3.70699C1.21489 1.92788 2.64047 0.692272 4.43397 0.454182C7.38244 0.0627671 10.3432 -0.0504852 13.3125 0.019374C15.2558 0.0650948 17.1939 0.196034 19.1182 0.487769C20.8637 0.752413 22.2485 2.05375 22.5451 3.81011C22.741 4.97052 22.825 6.14992 22.9592 7.3208C22.9725 7.43639 22.9867 7.55187 23.0004 7.66741C23.0004 8.3756 23.0004 9.0838 23.0004 9.792ZM11.7012 16.0426C13.8944 16.0362 16.1221 15.9534 18.7468 15.5978C20.0217 15.4252 20.9579 14.5824 21.179 13.3291C21.4137 11.9989 21.5523 10.6572 21.5847 9.30589C21.6264 7.5749 21.4731 5.85862 21.1851 4.15337C20.9872 2.98171 20.1403 2.09402 18.9658 1.90175C17.0889 1.59451 15.1924 1.47968 13.2933 1.43489C10.3756 1.36607 7.4657 1.4701 4.56942 1.86654C3.42441 2.02327 2.45315 2.83734 2.246 3.98049C2.0624 4.99376 1.93551 6.02135 1.85338 7.04822C1.68555 9.14641 1.83209 11.2347 2.20086 13.3045C2.39189 14.3766 3.00739 15.1302 4.05392 15.4681C4.43428 15.5909 4.84581 15.6259 5.24626 15.6756C7.38952 15.9416 9.54235 16.0475 11.7012 16.0426Z" fill="#0063EE"/>
										<path d="M8.68338 8.72569C8.6833 7.65678 8.68199 6.58786 8.68377 5.51895C8.68492 4.83319 9.24042 4.51517 9.82944 4.86643C11.6231 5.93609 13.4158 7.00749 15.2067 8.08191C15.7718 8.42099 15.7698 9.05211 15.2051 9.38952C13.4186 10.4569 11.6297 11.5202 9.84401 12.5888C9.59377 12.7386 9.34238 12.803 9.0723 12.6642C8.80499 12.5269 8.68202 12.3003 8.68262 11.9988C8.68477 10.9078 8.68347 9.81672 8.68338 8.72569ZM10.1049 10.7835C11.2576 10.0952 12.3817 9.42403 13.5385 8.7333C12.3769 8.03803 11.2526 7.36504 10.1049 6.67807C10.1049 8.0575 10.1049 9.39705 10.1049 10.7835Z" fill="#0063EE"/>
									</svg>
									</span>{ whiteName } <span className="try-free-mobile"><i
										className="ic-arrow-right-blue"></i></span></a> : ''}
						<button className="goDown">
							<img src="/wp-content/themes/aso/images/dest/arrow-down.svg" alt="Go down" className="goDown__img"/>
						</button>
					</div>
				</div>
			</div>
		);
	}
} );
