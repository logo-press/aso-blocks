/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const IconButton = wp.components.IconButton;

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';



/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/main-banner-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Main banner' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h1'
		},
		tip: {
			type: 'array',
			source: 'children',
			selector: 'p'
        },
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, tip, greenURL, greenName, greenTRG }, setAttributes } = props;
        
		const TEMPLATE = [
			[ 'trilogo/icon-1', {}], 
			[ 'trilogo/icon-2', {}], 
			[ 'trilogo/icon-3', {}], 
			[ 'trilogo/icon-4', {}], 
			[ 'trilogo/icon-5', {}],
			[ 'trilogo/icon-6', {}],
			[ 'trilogo/icon-7', {}]
		];

		const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};

		return (
			<div  className="main-banner ver1">
				<div className="container">            	
					<RichText
						tagName="h1"	
						className="page-title"				
						placeholder={ __('Введите заголовок баннера') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
					<RichText
						tagName="p"	
						className="banner__tip"
						placeholder={ __('Анализ конкурентов, создание АСО...') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTip}
						value={ tip }/>
						<div className="buttons">
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="div"		
									className="link-url"
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeGreenURL}
									value={ greenURL }/>
								<ToggleControl 
									checked={ greenTRG }
									className="link-url"
									onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
							</div>							
							<TextControl
								tagName="span"		
								className="link-name green-link"			
								placeholder={ __('Попробовать бесплатно') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeGreenName}
								value={ greenName }/>							
						</div>					
					<div className="icons-block">
						<InnerBlocks 
							allowedBlocks={ ['trilogo/icon-1', 
							'trilogo/icon-2', 'trilogo/icon-3', 'trilogo/icon-4', 'trilogo/icon-5', 'trilogo/icon-6', 'trilogo/icon-7'
						] }
							template={ TEMPLATE }/>
					</div>                  
				</div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, tip, greenURL, greenName, greenTRG }, setAttributes } = props;
		return (
			<div className="main-banner-1">
				<div className="container">	
					<div className="main-banner ver1">			
						<div className="main-banner__content">
							<RichText.Content 
								tagName='h1' 
								className="main-banner__title"
								value={ title } />
							<RichText.Content 
								tagName='p' 
								className="main-banner__subtitle"
								value={ tip } />
							{ greenName && greenURL && !greenTRG?
							<a href={ greenURL } className="green-link">{ greenName } <span className="try-free-mobile"><i
										className="ic-arrow-right-white"></i></span></a> : ''}
							{ greenName && greenURL && greenTRG?
							<a href={ greenURL } className="green-link" target="_blank" rel="noopener noreferrer">{ greenName } <span className="try-free-mobile"><i
										className="ic-arrow-right-white"></i></span></a> : ''}
						</div>
						<div className="main-banner__grid">
							<InnerBlocks.Content />	
						</div>
						<button className="goDown">
							<img src="/wp-content/themes/aso/images/dest/arrow-down.svg" alt="Go down" className="goDown__img"/>
						</button>
					</div>
				</div>
			</div>
		);
	}
} );
