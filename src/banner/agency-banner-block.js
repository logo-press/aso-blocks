/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

const el = wp.element.createElement;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/agency-banner-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Banner on agency page' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
        title: {
			type: 'array',
			source: 'children',
			selector: 'h1'
		},
        media: {
            type: 'object',
            default: {},
        },
        tip: {
			type: 'string'
        },
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false,
			
        },
        blueURL: {
			type: 'url'
        },
        blueName: {
			type: 'string'
        },
        blueTRG: {
			type: 'boolean',
			default: true
        },
        blueModal: {
			type: 'boolean',
			default: false
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, title, tip, greenURL, greenName, greenTRG, greenModal, blueURL, blueName, blueTRG, blueModal  }, setAttributes} = props;
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
        
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};		
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
		};
		const onChangeBlueURL = ( newBlueURL ) => {
            props.setAttributes( { blueURL: newBlueURL } );
		};
		const onChangeBlueName = ( newBlueName ) => {
            props.setAttributes( { blueName: newBlueName } );
		};

		return (

            <div className="banner banner__agency">
                <div className="container">
                    <RichText
						tagName="h1"	
						className="page-title"				
						placeholder={ __('Введите заголовок баннера') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>	
                    <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
						style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
							<MediaUpload
							onSelect={ onSelectImage }
							type='image'
							value={ media.id} 
							render={ function(obj) {
								return <Button
								className={ media.id ? 'image-button' : 'button button-large' }
								onClick={ obj.open }>
									{ !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
								</Button>
								}
							}/>
					</div>
                    <TextControl
						tagName="span"		
						className="banner__tip"
						placeholder={ __('Текст под картинкой') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTip}
						value={ tip }/>
					<div className="agency-buttons">
						<div className="agency-btn">
							<ToggleControl 
								label="Modal window"
								checked={ greenModal }
								className="link-url"
								onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="span"	
									className="link-url banner__button"	
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeGreenURL}
									value={ greenURL }/>
								<ToggleControl 
									checked={ greenTRG }
									className="link-url"
									onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
							</div>
							<TextControl
								tagName="span"		
								className="link-name green-link"			
								placeholder={ __('Название кнопки') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeGreenName}
								value={ greenName }/>	
						</div>		
						<div className="agency-btn">
							<ToggleControl 
								label="Modal window"
								checked={ blueModal }
								className="link-url"
								onChange={(newBlueModal) => setAttributes({ blueModal: newBlueModal})} />
							<div className="url-group">
								<IconButton icon="admin-links"
									className="link-btn"></IconButton>
								<TextControl
									tagName="span"	
									className="link-url banner__button"	
									placeholder={ __('Ссылка на страницу') }
									keepPlaceholderOnFocus= {true}
									onChange= {onChangeBlueURL}
									value={ blueURL }/>
								<ToggleControl 
									checked={ blueTRG }
									className="link-url"
									onChange={(newBlueTRG) => setAttributes({ blueTRG: newBlueTRG})} />
							</div>
							<TextControl
								tagName="span"		
								className="link-name blue-link"			
								placeholder={ __('Название кнопки') }
								keepPlaceholderOnFocus= {true}
								onChange= {onChangeBlueName}
								value={ blueName }/>		
						</div>	
                    </div>
                </div>                			
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { media, title, tip, greenURL, greenName, greenTRG, greenModal, blueURL, blueName, blueTRG, blueModal  }} = props;
		return (
            <div className="banner banner__agency">
                <div className="container">
                    <RichText.Content 
                        tagName='h1' 
                        className="banner__title"
                        value={ title } />
					{ media.id ?
					<div className="banner__img">
						<img src={ media.url } alt={ media.alt ? media.alt : 'Agency Banner'}/>
					</div>  : ''}
					{ tip ?
                    <div className="banner__tip">{ tip }</div>  : ''}                    
					<div className="banner__button">
						{ greenName && greenModal ?
						<button data-micromodal-trigger="modal-1" className="green-link">{ greenName }</button> : 
						greenName && greenURL && !greenTRG?
                        <a href={ greenURL } className="green-link">{ greenName }</a> : 
						greenName && greenURL && greenTRG?
                        <a href={ greenURL } className="green-link"  target="_blank" rel="noopener noreferrer">{ greenName }</a> : ''}
						
						{ blueName && blueModal ?
                        <button data-micromodal-trigger="modal-1" className="blue-link">{ blueName }</button> :
						blueName && blueURL && !blueTRG?
                        <a href={ blueURL } className="blue-link">{ blueName }</a> : 
						blueName && blueURL && blueTRG?
                        <a href={ blueURL } className="blue-link"  target="_blank" rel="noopener noreferrer">{ blueName }</a> : ''}
                    </div>
                </div>
            </div>
		);
	}
} );
