/**
 * BLOCK: block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton,
	RadioControl
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/tool-services', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Tool' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	parent: ['trilogo/tools-services-block'],
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		title: {
			type: 'string'
        },
        media: {
			type: 'object',
			default: {}
		},
        greenURL: {
			type: 'url'
        },
        greenName: {
			type: 'string'
        },
        greenTRG: {
			type: 'boolean',
			default: true
        },
        greenModal: {
			type: 'boolean',
			default: false
        },
		color: {
			type: 'string',
			default: ''
        },
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, media, greenName, greenURL, greenTRG, color, greenModal }, setAttributes} = props;
        
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
		};
		const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
        };
		const onChangeColor = (newColor) => {
			props.setAttributes({ color: newColor })
		};
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (  
            <div className="tools__item">
                <TextControl
					tagName="span"	
					className="tool-name"	
                    placeholder={ __('Введите название инструмента') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>		
				<RadioControl
					label="Ribbon color"
					selected={ color }
					options={ [
						{ label: 'Pink', value: 'ribbon-pink' },
						{ label: 'Purple', value: 'ribbon-purple' },
						{ label: 'Yellow', value: 'ribbon-yellow' },
					] }
					onChange={ onChangeColor } />	
				<div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                    style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                        <MediaUpload
                        onSelect={ onSelectImage }
                        type='image'
                        value={ media.id} 
                        render={ function(obj) {
                            return <Button
                            className={ media.id ? 'image-button' : 'button button-large' }
                            onClick={ obj.open }>
                                { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                            </Button>
                            }
                        }/>
                </div>
				<div className="buttons">
					<ToggleControl 
						label="Modal window"
						checked={ greenModal }
						className="link-url"
						onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal})} />
					<div className="url-group">
						<IconButton icon="admin-links"
							className="link-btn"></IconButton>								
						<TextControl
							tagName="span"	
							className="link-url watch-link"	
							placeholder={ __('Ссылка') }
							keepPlaceholderOnFocus= {true}
							onChange= {onChangeGreenURL}
							value={ greenURL }/>
						<ToggleControl 
							checked={ greenTRG }
							className="link-url"
							onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG})} />
					</div>	
					<TextControl
						tagName="span"		
						className="link-name watch-tools"
						placeholder={ __('Посмотреть инструменты') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeGreenName}
						value={ greenName }/>
				</div>	
            </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		const { attributes: { title, media, greenName, greenURL, greenTRG, color, greenModal }} = props;
		return (
            <div className="tools__item">
				{ media.id ?
				<div className={"tool__ribbon " + color }>
					<img src={ media.url } alt={ media.alt ? media.alt : media.title }/>
				</div> : ''}
                <span className="tools__item-title">{ title }</span>
				{ greenName ?
                <div className="tools__item-button">
					{ greenModal ?
					<button data-micromodal-trigger="modal-1" className="blue-link">{ greenName }</button> :
					greenURL && !greenTRG ?
                    <a href={ greenURL } className="blue-link">{ greenName }</a> : 
					greenURL && greenTRG ?
					<a href={ greenURL } className="blue-link"  target="_blank" rel="noopener noreferrer">{ greenName }</a> : '' }
                </div> : ''}
            </div>                
		);
	},
} );

        