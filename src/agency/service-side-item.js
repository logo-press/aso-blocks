/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	Toolbarside,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/service-side-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Item' ), // Block title.
    parent: ['trilogo/service-side'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
        price: {
            type: 'string'
        },
        desc: {
            type: 'string'
        },
        soon: {
            type: 'boolean',
            default: false
        }
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { price, desc, soon }, setAttributes} = props;

        const onChangeSoon = ( newSoon ) => {
            props.setAttributes( { soon: newSoon } );
        };
        const onChangePrice = ( newPrice ) => {
            props.setAttributes( { price: newPrice } );
        };
        const onChangeDesc = ( newDesc ) => {
            props.setAttributes( { desc: newDesc } );
        };
		return (
            <div className="service__side-item">                
                <TextControl
                    tagName="span"		
                    className=""		
                    placeholder={ __('Описание') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeDesc}
                    value={ desc }/>
                <div className="prices">
                    <ToggleControl 
                        checked={ soon }
                        className="link-url"
                        onChange={(newSoon) => setAttributes({ soon: newSoon })} />
                    <TextControl
                        tagName="span"		
                        className="disc-pr"		
                        placeholder={ __('Цена') }
                        keepPlaceholderOnFocus= {true}
                        onChange= {onChangePrice}
                        value={ price }/>
                </div>                		
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { soon, price, desc }} = props;

		return (  
            !soon ?          
			<div className="service__side-item">
                { desc ?
                <div className="service__side-desc">{ desc }</div> : ''}
                { price ?
                <div className="service__side-price ">{ price }</div> : ''}                
            </div>:
			<div className="service__side-item ">
                { desc ?
                <div className="service__side-desc">{ desc }</div> : ''}
                <div className="service__soon"></div>
            </div> 
		);
	}
} );
