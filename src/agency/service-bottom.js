/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	Toolbarbottom,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/service-bottom', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Service bottom' ), // Block title.
    parent: ['trilogo/service-block'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
        },
        price: {
            type: 'string'
        },
        desc: {
            type: 'string'
        },
        btnName: {
            type: 'string'
        },
        btnURL: {
            type: 'string'
        },
        btnTRG: {
            type: 'boolean',
            default: true
        }, 
        btnModal: {
            type: 'boolean',
            default: false
        }, 
        greenName: {
            type: 'string'
        },
        greenURL: {
            type: 'string'
        },
        greenTRG: {
            type: 'boolean',
            default: true
        }, 
        greenModal: {
            type: 'boolean',
            default: false
        }, 
        media: {
            type: 'object',
            default: {}
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { 
            title, 
            desc, 
            btnName, 
            btnURL, 
            btnTRG,
            btnModal,
            greenName, 
            greenURL, 
            greenTRG,
            greenModal,
            media
         }, setAttributes} = props;
         const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
        };
        const onChangeDesc = ( newDesc ) => {
            props.setAttributes( { desc: newDesc } );
        };
        const onChangeBtnName = ( newBtnName ) => {
            props.setAttributes( { btnName: newBtnName } );
        };
        const onChangeBtnURL = ( newBtnURL ) => {
            props.setAttributes( { btnURL: newBtnURL } );
		};
        const onChangeGreenName = ( newGreenName ) => {
            props.setAttributes( { greenName: newGreenName } );
        };
        const onChangeGreenURL = ( newGreenURL ) => {
            props.setAttributes( { greenURL: newGreenURL } );
        };
        var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (
            <div className="service__bottom">
				<RichText
                    tagName="span"	
                    className="block-title"
                    placeholder={ __('Заголовок') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>
                <TextControl
                    tagName="span"		
                    className=""		
                    placeholder={ __('Описание') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeDesc}
                    value={ desc }/>
                <div className="bottom-row">
                    <div className="buttons">
                        <div className="button">
                            <ToggleControl 
                                checked={ btnModal}
                                className="link-url"
                                onChange={(newBtnModal) => setAttributes({ btnModal: newBtnModal })} />
                            <div className="url-group">
                                <IconButton icon="admin-links"
                                    className="link-btn"></IconButton>
                                <TextControl
                                    tagName="span"		
                                    className="link-url"
                                    placeholder={ __('Ссылка') }
                                    keepPlaceholderOnFocus= {true}
                                    onChange= {onChangeBtnURL}
                                    value={ btnURL }/>
                                <ToggleControl 
                                    checked={ btnTRG }
                                    className="link-url"
                                    onChange={(newBtnTRG) => setAttributes({ btnTRG: newBtnTRG })} />
                            </div>
                            <TextControl
                                tagName="span"		
                                className="link-name blue-link"
                                placeholder={ __('Название кнопки') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeBtnName}
                                value={ btnName }/>			
                        </div> 
                        <div className="button">
                            <div className="url-group">
                                <ToggleControl 
                                    label="Modal window"
                                    checked={ greenModal }
                                    className="link-url"
                                    onChange={(newGreenModal) => setAttributes({ greenModal: newGreenModal })} />
                                <IconButton icon="admin-links"
                                    className="link-btn"></IconButton>
                                <TextControl
                                    tagName="span"		
                                    className="link-url"
                                    placeholder={ __('Ссылка') }
                                    keepPlaceholderOnFocus= {true}
                                    onChange= {onChangeGreenURL}
                                    value={ greenURL }/>
                                <ToggleControl 
                                    checked={ greenTRG }
                                    className="link-url"
                                    onChange={(newGreenTRG) => setAttributes({ greenTRG: newGreenTRG })} />
                            </div>
                            <TextControl
                                tagName="span"		
                                className="link-name green-link"
                                placeholder={ __('Название кнопки') }
                                keepPlaceholderOnFocus= {true}
                                onChange= {onChangeGreenName}
                                value={ greenName }/>			
                        </div> 
                    </div> 
                    <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                        style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                            <MediaUpload
                            onSelect={ onSelectImage }
                            type='image'
                            value={ media.id} 
                            render={ function(obj) {
                                return <Button
                                className={ media.id ? 'image-button' : 'button button-large' }
                                onClick={ obj.open }>
                                    { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                                </Button>
                                }
                            }/>
                    </div>
                </div>	                
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { 
            title, 
            desc, 
            btnName, 
            btnURL, 
            btnTRG,
            btnModal,
            greenName, 
            greenURL, 
            greenTRG,
            greenModal,
            media
         }} = props;
		return (            
			<div className="service__bottom">
				<div className="service__bottom-content">
                    { title ?
                    <RichText.Content
                        tagName="span"
                        className="service__bottom-title"
                        value={ title }/> : ''}
                    { desc ?
                    <div className="service__bottom-desc">{ desc }</div> : ''}
                </div>
                <div className="service__bottom-wrap">
                <div className="service__buttons">
                    { btnModal && btnName ?
                     <button data-micromodal-trigger="modal-1" className="blue-link">{ btnName }</button> :
                    btnURL && btnName && btnTRG ?
                    <div className="service__buttons-item">
                        <a href={ btnURL } className="blue-link" target="_blank" rel="noopener noreferrer">{ btnName }</a>
                    </div> :
                    btnURL && btnName && !btnTRG ?
                    <div className="service__buttons-item">
                        <a href={ btnURL } className="blue-link">{ btnName }</a>
                    </div> : ''}
                    { greenModal && greenName ?
                    <button data-micromodal-trigger="modal-1" className="green-link middle">{ greenName }</button> :
                    
                    greenURL && greenName && greenTRG ?
                    <div className="service__buttons-item">
                        <a href={ greenURL } className="green-link" target="_blank" rel="noopener noreferrer">{ greenName }</a>
                    </div> : 
                    greenURL && greenName && !greenTRG ?
                    <div className="service__buttons-item">
                        <a href={ greenURL } className="green-link">{ greenName }</a>
                    </div> : ''}
                </div>    
                { media.id ?
                <div className="service__image">
                    <img src={ media.url } alt={ media.alt ? media.alt : 'ASO consultation'}/>
                </div> : ''}          
                </div>
			</div>
		);
	}
} );
