/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/service-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Service item' ), // Block title.
    parent: ['trilogo/service-items'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
        },
        price: {
            type: 'string'
        },
        desc: {
            type: 'string'
        },
        btnName: {
            type: 'string'
        },
        btnURL: {
            type: 'string'
        },
        btnTRG: {
            type: 'boolean',
            default: true
        }, 
        btnModal: {
            type: 'boolean',
            default: false
        }, 
        col: {
            type: 'boolean',
            default: true
        }
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { title, price, desc, btnName, btnURL, btnTRG, btnModal, col }, setAttributes} = props;

        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
        };
        const onChangePrice = ( newPrice ) => {
            props.setAttributes( { price: newPrice } );
        };
        const onChangeDesc = ( newDesc ) => {
            props.setAttributes( { desc: newDesc } );
        };
        const onChangeBtnName = ( newBtnName ) => {
            props.setAttributes( { btnName: newBtnName } );
        };
        const onChangeBtnURL = ( newBtnURL ) => {
            props.setAttributes( { btnURL: newBtnURL } );
		};
		return (
            <div className="service__list-item">
                <ToggleControl 
                    checked={ col }
                    className="link-url"
                    onChange={(newCol) => setAttributes({ col: newCol })} />
				<RichText
                    tagName="span"	
                    className="block-title"
                    placeholder={ __('Заголовок блока') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTitle}
                    value={ title }/>
                <TextControl
                    tagName="span"		
                    className="disc-pr"		
                    placeholder={ __('Цена') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangePrice}
                    value={ price }/>
                <TextControl
                    tagName="span"		
                    className=""		
                    placeholder={ __('Описание') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeDesc}
                    value={ desc }/>	
                <div className="buttons">
                    <ToggleControl 
                        label="Modal window"
                        checked={ btnModal }
                        className="link-url"
                        onChange={(newBtnModal) => setAttributes({ btnModal: newBtnModal })} />
                    <div className="url-group">
                        <IconButton icon="admin-links"
                            className="link-btn"></IconButton>
                        <TextControl
                            tagName="span"		
                            className="link-url buy-name"
                            placeholder={ __('Ссылка') }
                            keepPlaceholderOnFocus= {true}
                            onChange= {onChangeBtnURL}
                            value={ btnURL }/>
                        <ToggleControl 
                            checked={ btnTRG }
                            className="link-url"
                            onChange={(newBtnTRG) => setAttributes({ btnTRG: newBtnTRG })} />
                    </div>
                    <TextControl
                        tagName="span"		
                        className="link-name blue-link"
                        placeholder={ __('Название кнопки') }
                        keepPlaceholderOnFocus= {true}
                        onChange= {onChangeBtnName}
                        value={ btnName }/>			
                </div>   
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { title, price, desc, btnName, btnURL, btnTRG, btnModal, col }} = props;

		return (            
			<div className={ col ? "service__list-item" : "service__list-item right"}>
                <div className="service__item-content">
                    { title ?
                    <RichText.Content
                        tagName="span"
                        className="service__block-title"
                        value={ title }/> : ''}
                        { price ?
                    <div className="service__item-price">{ price }</div> : ''}
                    { desc ?
                    <div className="service__item-desc">{ desc }</div> : ''}
                </div>
                { btnModal && btnName ?
                <button data-micromodal-trigger="modal-1" className="blue-link">{ btnName }</button> :

                btnURL && btnName && btnTRG ?
                <div className="service__item-button">
                    <a href={ btnURL } className="blue-link" target="_blank" rel="noopener noreferrer">{ btnName }</a>
                </div> : 
                btnURL && btnName && !btnTRG ?
                <div className="service__item-button">
                    <a href={ btnURL } className="blue-link">{ btnName }</a>
                </div> : ''}
			</div>
		);
	}
} );
