/**
 * BLOCK: block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/agency-desc-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Description block Agency page' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
		media: {
            type: 'object',
            default: {},
        },
		title: {
			type: 'array',
			source: 'children',
			selector: 'span'
        },
        tip: {
			type: 'array',
			source: 'children',
			selector: 'p'
        }
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { media, title, tip }} = props;
        
        const onChangeTitle = ( newTitle ) => {
            props.setAttributes( { title: newTitle } );
		};
		const onChangeTip = ( newTip ) => {
            props.setAttributes( { tip: newTip } );
		};
		var onSelectImage = function (media) {
			props.setAttributes({ media: media })
		};
		return (  
            <div className="agency">
                <div className={ media.id ? 'organic-profile-image image-active' : 'organic-profile-image image-inactive' }
                    style={ media.id ? { Image: 'url(' + media.url + ')' } : {} }>
                        <MediaUpload
                        onSelect={ onSelectImage }
                        type='image'
                        value={ media.id} 
                        render={ function(obj) {
                            return <Button
                            className={ media.id ? 'image-button' : 'button button-large' }
                            onClick={ obj.open }>
                                { !media.id ? __('Upload Image')  : <img src={ media.url } alt={ media.alt }/> }
                            </Button>
                            }
                        }/>
                </div>
				<div className="title-wrapper">
					<RichText
						tagName="span"	
						className="page-title"	
						placeholder={ __('Заголовок блока') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeTitle}
						value={ title }/>
				</div>
				<div className="tip-wrapper">		
                <RichText
					tagName="span"	
					className="tip"
                    placeholder={ __('Описание блока') }
                    keepPlaceholderOnFocus= {true}
                    onChange= {onChangeTip}
                    value={ tip }/>
				</div>
            </div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
        const { attributes: { media, title, tip }} = props;
		return (
            <div  className="agency">
                <div className="container">
                    <div className="agency__image">
                        { media.id ?
                        <img src={ media.url } alt={ media.alt ? media.alt : 'ASO agensy'}/> : ''}
                    </div>
                    <RichText.Content 
                        tagName='span'
                        className='page-title' 
                        value={ title } />
                    <RichText.Content 
                        tagName='p'
                        className='agency__desc' 
                        value={ tip } />
                </div>
            </div>                 
		);
	},
} );

        