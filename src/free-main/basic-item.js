/**
 * BLOCK: blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss'; 

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

import { 
	Toolbar,
	ToolbarButton,
	ToolbarItem,
	ToggleControl, 
	Button,
	TextControl,
	IconButton
 } from '@wordpress/components';

import {
    useBlockProps,
    RichText,
    AlignmentToolbar,
	BlockControls,
	InspectorControls,
	MediaUpload, MediaUploadCheck,
	InnerBlocks
} from '@wordpress/block-editor';
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'trilogo/basic-item2', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'TriloGO Basic rate item' ), // Block title.
    parent: ['trilogo/free-main-page'],
    icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	attributes: {
        itemName: {
			type: 'string'
		},
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
        const { attributes: { itemName }, setAttributes} = props;

        const onChangeItemURL = ( newItemURL ) => {
            props.setAttributes( { itemURL: newItemURL } );
		};
		const onChangeItemName = ( newItemName ) => {
            props.setAttributes( { itemName: newItemName } );
		};
		return (
            <div  className="basic-cont">
				<div className="buttons">
					<TextControl
						tagName="span"	
						className="link-name"	
						placeholder={ __('Название тарифа') }
						keepPlaceholderOnFocus= {true}
						onChange= {onChangeItemName}
						value={ itemName }/>
				</div>
			</div>		
		);
	},
	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Frontend HTML.
	 */
	save: ( props ) => {
		return (       
			
			<li  className="basic__pos-item">
				{ props.attributes.itemName ?
				//  && props.attributes.itemURL && !props.attributes.greenTRG ?
				// <a href={ props.attributes.itemURL } className="basic__pos-link">
					props.attributes.itemName : ''}
				{/* { props.attributes.itemName && props.attributes.itemURL && props.attributes.greenTRG ?
				<a href={ props.attributes.itemURL } className="basic__pos-link"  target="_blank" rel="noopener noreferrer">{ props.attributes.itemName }</a> : ''}  */}
			</li>
			
		);
	}
} );
